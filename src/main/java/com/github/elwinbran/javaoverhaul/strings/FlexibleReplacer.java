/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.strings;

/**
 * This class can replace pieces of strings inside other strings with new strings.
 * Simply put, replaces strings inside strings.
 * 
 * TODO: is this class not inherently unsafe? If so, it needs to move to another
 * package and use SafeReturns and formattedStrings.
 * 
 * @author Elwin Slokker
 */
public interface FlexibleReplacer
{
    /**
     * Changes a string by replacing based on the given arguments and returns 
     * the changed string.
     * 
     * @param source The string that has (a) part(s) that need to be replaced.
     * @param target The part(s) that need to be replaced.
     * @param replacement The string that will be put in the spots where 
     * {@code target} was before in {@code source}.
     * @return A string that looks like the {@code source} string but has
     * all occurences of {@code target} removed and replaced {@code replacement}.
     */
    public abstract FormattedString replace(FormattedString source, 
            FormattedString target, FormattedString replacement);
}
