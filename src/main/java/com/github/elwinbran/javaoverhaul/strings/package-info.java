/**
 * This project/package contains a interfaces that represent textual objects
 * that are more maintainable than the default Java String and Character.
 */
package com.github.elwinbran.javaoverhaul.strings;
